-- chikun :: 3015
-- Load all fonts


local fnt = {

	splash  = love.graphics.newFont(... .. "/exo2.otf", 48),
	text    = love.graphics.newFont(... .. "/gbb.ttf", 14),
	mm_text = love.graphics.newFont(... .. "/gbb.ttf", 28)
	
}

fnt.text:setFilter('nearest', 'nearest', 0)
fnt.mm_text:setFilter('nearest', 'nearest', 0)

return fnt
