-- chikun :: 2014-2015
-- Loads all states from the current folder and creates control functions


local current_dir = ...


-- State prototype --

-- State class on which all others are based
PrototypeState = class(function(new_class) end)

function PrototypeState:create() end    -- On state create
function PrototypeState:update(dt) end  -- On state update, has delta time
function PrototypeState:draw() end      -- On state draw
function PrototypeState:kill() end      -- On state kill


-- Create state table
local State = class(function(new_class)

		new_class.current = ""
		new_class.states  = {}
	end)


-- State management functions --


--[[
	Kill old state and load a new one.
	INPUT:  State to change to.
	OUTPUT: Nothing.
]]
function State:change(new_state)

	self.states[self.current]:kill()
	self:load(new_state)
end


--[[
	Loads new state. Reloads current state if argument omitted.
	INPUT:  New state to load, or nothing.
	OUTPUT: Nothing.
]]
function State:load(new_state)

	self.current = new_state or self.current
	self.states[self.current]:create()
end


--[[
	Kill old state and sets another.
	INPUT:  State to move to.
	OUTPUT: Nothing.
]]
function State:set(new_state)

	self.states[self.current].kill()
	self.current = new_state
end


--[[
	Update current state.
	INPUT:  Delta time.
	OUTPUT: Nothing.
]]
function State:update(dt, given_state)

	self.states[given_state or self.current]:update(dt)
end


--[[
	Draw current state.
	INPUT:  Nothing.
	OUTPUT: Nothing.
]]
function State:draw(given_state)

	self.states[given_state or self.current]:draw()
end


-- State loading --

local state_manager = State()

-- Get a table of files / subdirs from dir
local items = love.filesystem.getDirectoryItems(...)

-- Iterate through all files and load them into states
for key, val in ipairs(items) do

	-- Load file as long as it isn't init.lua
	if (val ~= "init.lua") then

		local name = val:sub(1, -5)
		state_manager.states[name] = require(current_dir .. "/" .. name)
	end
end


return state_manager
