-- chikun :: 2016
-- Text state


-- Temporary state, removed at end of script
local TextState = class(PrototypeClass, function(new_class) end)


-- On state create
function TextState:create()

	self.text = txt.intro       -- Current text file
	self.text_step = 1          -- Current text section
	self.text_roll = 0          -- How many chars have been rolled
	self.player_name = ""		-- players choosen name
end


-- On state update
function TextState:update(dt)

	-- Learn current text
	local current_text = self:currentText()

	-- Update roll of text
	self.text_roll = math.min(self.text_roll + 192 * dt, current_text:len())

    -- Play sound while text is rolling
    if (self.text_roll < current_text:len()) then
        sfx.text_roll:setVolume(0.2)
		sfx.text_roll:play()
    end

	-- If the action button is pressed, advance dialog
	if (self.text[self.text_step].choices) then

		if (ci:wasPressed('down')) then

			self.choice = math.min(#self.text[self.text_step].choices, self.choice + 1)
		elseif (ci:wasPressed('up')) then

			self.choice = math.max(self.choice - 1, 1)
		end
	end

	if (ci:wasPressed('a')) then

		self:advanceText()

        -- Dialogue advancement sound
        sfx.ok:setVolume(0.2)
        sfx.ok:play()

        -- self.music:setVolume(1)
        -- self.music:play()
	end
end


-- On state draw
function TextState:draw()

	-- Learn current text and sub_num
	local current_text = self:currentText()
	local sub_num = math.floor(self.text_roll)
	local step = self.text[self.text_step]

	-- if current step has background then interpret
	if (step.background) then

		if (step.background == "clear") then

			self.current_bg = nil
		else

			self.current_bg = step.background
		end
	end

	-- set background to black
	lg.setColor(255, 255, 255)

	-- draw current background if exists
	if (self.current_bg) then

		lg.draw(self.current_bg)
	else

		cs.states['play']:draw()
	end

	-- if current step has music then interpret
	if (step.music) then

		if (self.current_music == "clear") then

			self.current_music = nil
		else

			self.current_music = step.music
		end
	end

	-- play music if it exists
	if (self.current_music) then

		if (step.newmusic) then

			la.stop(step.newmusic)
		end
        la.setVolume(0.1)
		la.play(self.current_music)
	end

	-- select name of player
	if (self.choice) then

		self:selectName(step.choices)
	end

	-- if current step has speaker then interpret
	if (step.speaker) then

		if (step.speaker == "none") then

			self.current_speak = nil
		else

			self.current_speak = step.speaker
		end
	end

	self:drawText(current_text:sub(1, math.floor(self.text_roll)))

	self:setSpeakerName(self.current_speak)


	-- display input button for controllers
	if (self.text_roll == current_text:len()) then

		lg.draw(gfx.buttons[ci.scheme].a, 296, 116)
	end
end

--
function TextState:drawText(this_text)

	-- Draw back of text box
	lg.setColor(0, 0, 0)
	lg.rectangle('fill', 4, 140, 312, 36, 2, 2)

	-- Draw front of text box
	lg.setColor(255, 255, 255)
	lg.rectangle('line', 4, 140, 312, 36, 4, 4)


	-- Draw text in box
	lg.setFont(cf.text)
	lg.printf(this_text:sub(1, sub_num), 8, 144, 304, 'left')


end

-- sets the name of the current speaker
function TextState:setSpeakerName(current_speaker)

	-- print name of speaking char above textbox
	if (current_speaker) then

		-- text with shadow
		lg.setColor(0, 0, 0)
		lg.printf(current_speaker, 9, 125, 303)

		lg.setColor(255, 255, 255)
		lg.printf(current_speaker, 8, 124, 304)
	else

		-- play state
	end
end

function TextState:selectName(choices_table)

	if (choices_table) then
		-- Draw back of text box
		lg.setColor(0, 0, 0)
		lg.rectangle('fill', 80, 50, 60, 36, 2, 2)

		-- Draw front of text box
		lg.setColor(255, 255, 255)
		lg.rectangle('line', 80, 50, 60, 36, 4, 4)


		local x_pbox = 94
		local y_pbox = 52

		-- Draw text in box
		lg.setFont(cf.text)
		for k, v in ipairs(choices_table) do
			lg.setColor(255, 255, 255)
			if (k == self.choice) then
				lg.setColor(255, 100, 100)
			end

			lg.printf(v, x_pbox, y_pbox, 64, 'left')
			lg.printf("\n", x_pbox, y_pbox, 64, 'left')
			y_pbox = y_pbox + 10
		end
	end
end

-- Returns the current text
function TextState:currentText()

	return self.text[self.text_step].text:gsub("~~~", self:getPlayerName())
end

-- Get player name
function TextState:getPlayerName(data)

	return self.player_name or ""
end

-- Set player name
function TextState:setPlayerName(data)

	self.player_name = data
end

-- Advances text
function TextState:advanceText()

	-- Determine current text
	local current_text = self:currentText()

	-- If not all text has been rolled, roll it all
	if (self.text_roll < current_text:len()) then

		self.text_roll = current_text:len()

	-- Otherwise move to the next step
	else

		if (self.text[self.text_step].code) then

			self.text[self.text_step].code(self.choice)
		end

		-- Move forward one step, reset roll
		self.text_step = self.text_step + 1
		self.text_roll = 0

		-- If text step is larger than number of text sections
		if (self.text_step > #self.text) then

			cs:set(prev_state)
			-- Restart steps
			self.text_step = 1
		end

		-- If choices exist, set default to 1
		if (self.text[self.text_step].choices) then

			self.choice = 1
		else

			self.choice = nil
		end
	end
end


-- On state kill
function TextState:kill() end


-- Transfer data to state loading script
return TextState
