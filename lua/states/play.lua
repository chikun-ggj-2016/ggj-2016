-- chikun :: 2016
-- Main menu state


-- Temporary state, removed at end of script
local PlayState = class(PrototypeClass, function(new_class) end)


-- On state create
function PlayState:create()

	-- Create our beautiful player object
	self.player = Player()
	self.zazz = Zazz()

	-- Load up the test map
	self:setMap('main_1')
end

--[[


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@;;;'''''###+++++''';;;;;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@'''''''';;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@'''''''';;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@'''''''';;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@                @@@@@;;;;;;;;@@@@@@                @@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@                @@@@@;;;;;;;;@@@@@@                @@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@                      @@;;;;;;;;@@@                     @@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@                      @@;;;;;;;;@@@                     @@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@                      @@;;;;;;;;@@@                     @@@@@@@@@@@@@@
@@@@@@@@@@@@@@,,                      ,,@@@;;;@@,,,                     ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@,,                      ,,@@@;;;@@,,,                     ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@,,                      ,,@@@;;;@@,,,                     ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@``      ``@@@@@@@@```   ``@@@;;;@@```     ```@@@@@@@@```  ```@@@@@@@@@@@
@@@@@@@@@@@@@@``      ``@@@@@@@@```   ``@@@;;;@@```     ```@@@@@@@@```  ```@@@@@@@@@@@
@@@@@@@@@@@@@@``      @@@@@@@@@@@@@   ``@@@;;;@@```     @@@@@@@@@@@@@@  ```@@@@@@@@@@@
@@@@@@@@@@@@@@``      @@@@@@@@@@@@@   ``@@@;;;@@```     @@@@@@@@@@@@@@  ```@@@@@@@@@@@
@@@@@@@@@@@@@@``      @@@@@@@@@@@@@   ``@@@;;;@@```     @@@@@@@@@@@@@@  ```@@@@@@@@@@@
@@@@@@@@@@@@@@,,      @@@@@   ``@@@   ,,@@@;;;@@,,,     @@@@@@  ```@@@  ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@,,      @@@@@   ``@@@   ,,@@@;;;@@,,,     @@@@@@  ```@@@  ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@,,      @@@@@   ``@@@   ,,@@@;;;@@,,,     @@@@@@  ```@@@  ,,,@@@@@@@@@@@
@@@@@@@@@@@@@@@@      @@@@@```@@@@@   @@;;;;;;;;@@@     @@@@@@``@@@@@@  @@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@      @@@@@```@@@@@   @@;;;;;;;;@@@     @@@@@@``@@@@@@  @@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@   ``@@@@@@@@```@@@@@;;;;;;;;@@@@@@  ```@@@@@@@@```@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@   ``@@@@@@@@```@@@@@;;;;;;;;@@@@@@  ```@@@@@@@@```@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@   ``@@@@@@@@```@@@@@;;;;;;;;@@@@@@  ```@@@@@@@@```@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@;;:::;;;;;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@;;:::;;;;;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@,,``````,,@@@@@@;;:::;;;;;;;;@@@@@,,,`````,,,@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@'''@@@@@@@@@@;;;;;;;;;;;;;;;;;;;;;;;;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@'''@@@@@@@@@@;;;;;;;;;;;;;;;;;;;;;;;;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@::::::::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'''''@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@::::::::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'''''@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@::::::::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'''''@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@;;::::::::;;;;;;;;;;;;;;;;;;;''';;;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@;;::::::::;;;;;;;;;;;;;;;;;;;''';;;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@;;::::::::;;;;;;;;;;;;;;;;;;;''';;;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@;;::::::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@;;::::::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@@@@@@@@@;;;;;;;;;;;;;;;;@@@@@@@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@@@@@@@@@;;;;;;;;;;;;;;;;@@@@@@@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@@@@@@@@@;;;;;;;;;;;;;;;;@@@@@@@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@     @@@@@@@@@@@@@@@@@@@@@@     @@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@''@@@     @@@@@@@@@@@@@@@@@@@@@@     @@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                          @@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                          @@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@           hey            @@@@@@''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;@@@@@                      @@@@@'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;@@@@@                      @@@@@'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;@@@@@                      @@@@@'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@::::::@@@@@                @@@@@;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@::::::@@@@@                @@@@@;;;;;;''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::@@@@@@@@@@@@@@@@@@@@@@;;;;;'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::@@@@@@@@@@@@@@@@@@@@@@;;;;;'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::@@@@@@@@@@@@@@@@@@@@@@;;;;;'''''@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;'''@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;'''@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@;;;:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;'''@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@''':::::;;;;;;;;;;;;;;;;;;;;;;;;;;;'''@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@''':::::;;;;;;;;;;;;;;;;;;;;;;;;;;;'''@@@@@@@@@@@@@@@@@@@@@@@@




--]]

-- On state update
function PlayState:update(dt)

	self.action = nil

	-- Update player, supplying the map's collisions
	self.player:update(dt, self.collisions)
	self.zazz:update(dt)

	-- Find useful x and y of player
	local hitbox = self.player:getHitbox()
	local px, py = hitbox.x + hitbox.w / 2, hitbox.y + hitbox.h / 2

	--
	local dist = 12
	local port = {}

	if (not(self.lock and #self.enemies > 0)) then

		for key, door in ipairs(self.doors) do

			local dx, dy = door.x + door.w / 2, door.y + door.h / 2
			local door_dist = math.distance(dx, dy, px, py)

			if (door_dist < dist and door.to) then

				dist = door_dist
				port = door.to

				self.action = "Enter door"
			end
		end
	end

	if (self.lock and #self.enemies == 0 and self.lock.x > 0) then

		self.lock.x = self.lock.x - 100000
	end

	for key, enemy in ipairs(self.enemies) do

		enemy:update(dt, self.collisions)
	end

	for key, gromato in ipairs(self.gromatoes) do

		gromato:update(dt, self.collisions)
	end

	if (#port == 2 and ci:wasPressed('y')) then

		if (self.lock) then

			self.lock.x = self.lock.x + 100000
		end

		self:setMap(port[1], port[2])
	end

	if (self.tool_rec) then

		if (math.doesCollide(self.player:getHitbox(), self.tool_rec)) then

			if (self.tool_rec.name == "outro_pu") then

				swapState(txt.outro)
			else

				self.action = "Collect " .. self.tool.tool

				if (ci:wasPressed('y')) then

					self.tool_rec = nil

					self.player.unlocked[self.tool.tool] = true

					sfx.tool_unlocked:setVolume(0.2)
					sfx.tool_unlocked:play()

					for key, value in ipairs(cm.current.layers) do

						if (value.name == "important") then

							table.remove(value.objects, self.tool.key)
						end
					end

					self.tool = {}
				end
			end
		end
	end

	if (not self.action) then

		self.action = "Talk to Zazz"

		if (ci:wasPressed('y')) then

			swapState(txt.zazz[math.random(#txt.zazz)])
		end
	end
end


-- On state draw
function PlayState:draw()

	-- Calculate translation
	local trans_x, trans_y =
	    math.round(self.player.x + self.player.w / 2 - GAME_WIDTH / 2),
	    math.round(self.player.y + self.player.h / 2 - GAME_HEIGHT / 2)


	-- Set up render queue
	local render_queue = { }

	-- Add player to queue
	table.insert(render_queue, self.player)
	table.insert(render_queue, self.test)
	table.insert(render_queue, self.zazz)

	for key, enemy in ipairs(self.enemies) do

		table.insert(render_queue, enemy)
	end

	for key, gromato in ipairs(self.gromatoes) do

		table.insert(render_queue, gromato)
	end

	-- Sort render queue by y position
	table.sort(render_queue, function(t1, t2)
			return (t1.y + t1.h) < (t2.y + t2.h)
		end)


	-- Draw map and queue, translated to be fixed on the player's position
	lg.translate(-trans_x, -trans_y)
		lg.setColor(255, 255, 255)
		cm.draw()
		for key, object in ipairs(render_queue) do object:draw() end
		if (self.tool.tool) then
			lg.draw(gfx.weapon[self.tool.tool], self.tool.x, self.tool.y)
		end
	lg.translate(trans_x, trans_y)
	lg.setColor(255, 255, 255)

	if ('play' ~= cs.current) then return nil end

	local txt = " x " .. self.player.score

	lg.draw(gfx.items.grape_tomato, 8, 6, 0, 2)
	lg.setFont(cf.text)
	lg.setColor(0, 0, 0)
	lg.print(txt, 25, 9)
	lg.setColor(255, 255, 255)
	lg.print(txt, 24, 8)


	-- If a button is to be drawn on screen...
	if (self.action) then

		-- Draw the text for the button
		local text = self.action
		lg.setFont(cf.text)
		lg.setColor(0, 0, 0)
		lg.printf(text, 1, 7, 272, 'right')
		lg.setColor(255, 255, 255)
		lg.printf(text, 0, 6, 272, 'right')
	end

	lg.draw(gfx.buttons[ci.scheme].y, 276, 4)
	lg.draw(gfx.buttons[ci.scheme].b, 296, 24)
	lg.draw(gfx.buttons[ci.scheme].x, 256, 24)
	lg.draw(gfx.buttons[ci.scheme].a, 276, 44)

	if (self.player.unlocked.knife) then
		if (ci.scheme == "keys") then

			lg.setColor(85, 242, 77)
		end
		lg.draw(gfx.weapon.knife, 288, 56)
		lg.setColor(255, 255, 255)
	end
	if (self.player.unlocked.spoon) then
		if (ci.scheme == "keys") then

			lg.setColor(105, 105, 214)
		end
		lg.draw(gfx.weapon.spoon, 268, 36)
		lg.setColor(255, 255, 255)
	end
	if (self.player.unlocked.fork) then
		if (ci.scheme == "keys") then

			lg.setColor(247, 89, 72)
		end
		lg.draw(gfx.weapon.fork, 308, 36)
		lg.setColor(255, 255, 255)
	end

	if (#self.enemies > 0) then

		local boss = self.enemies[1]

		if (boss.health) then

			lg.draw(boss.image, 8, 140, 0, 0.5, 0.5)

			lg.setColor(200, 10, 10)
			lg.rectangle('fill', 48, 148, 100, 16)
			lg.setColor(10, 200, 10)
			lg.rectangle('fill', 48, 148, boss.health / 3 * 100, 16)
			lg.setColor(0, 0, 0)
			lg.rectangle('line', 48, 148, 100, 16)
		end
	end
end


-- Load a new map with an optional position
function PlayState:setMap(map_var, position)

	-- Set the saved current map
	cm.current = maps[map_var]

	-- Blank tables for all collisions and doors in this map
	self.collisions = { }
	self.doors = { }
	self.enemies = { }
	self.gromatoes = { }
	self.lock = nil
	self.tool = { }
	self.tool_rec = nil


	-- Interpret all map layers
	for key, layer in ipairs(cm.current.layers) do

		if (layer.name == "important") then

			for key, object in ipairs(layer.objects) do

				-- Spawn player at point if exists
				-- Will include conditionals soon
				if (object.name == "spawn" and not position) then

					self.player:moveTo(object)
					self.zazz:moveTo(object)
				elseif (object.name == "knife") then

					self.tool = {
						x = object.x,
						y = object.y,
						tool = "knife",
						key = key
					}
				elseif (object.name == "spoon") then

					self.tool = {
						x = object.x,
						y = object.y,
						tool = "spoon",
						key = key
					}
				elseif (object.name == "fork") then

					self.tool = {
						x = object.x,
						y = object.y,
						tool = "fork",
						key = key
					}
				elseif (object.name == "knife_pu" and
					    not self.player.unlocked.knife) then

					self.tool_rec = object
				elseif (object.name == "spoon_pu" and
						not self.player.unlocked.spoon) then

					self.tool_rec = object
				elseif (object.name == "fork_pu" and
						not self.player.unlocked.fork) then

					self.tool_rec = object
				elseif (object.name == "outro_pu") then

					self.tool_rec = object
				elseif (object.name == "lock") then

					self.lock = object
				end
			end

		-- Interpret the almighty collision layer
		elseif (layer.name == "collisions") then

			for key, object in ipairs(layer.objects) do

				table.insert(self.collisions, {
						x = object.x,
						y = object.y,
						w = object.w,
						h = object.h
					})
			end

		-- Interpret the almighty collision layer
		elseif (layer.name == "enemies") then

			for key, object in ipairs(layer.objects) do

				table.insert(self.enemies,
				             ent[object.name](object.x, object.y))
			end

		-- Become the doors layer
		elseif (layer.name == "doors") then

			for key, object in ipairs(layer.objects) do

				local tab = { }

				if (object.prop["to"]) then

					for i in object.prop["to"]:gmatch("%S+") do

						table.insert(tab, i)
					end
				else

					tab = nil
				end

				table.insert(self.doors, {
						name = object.name,
						x = object.x,
						y = object.y,
						w = object.w,
						h = object.h,
						to = tab
					})

				if (object.name == position) then

					self.player:moveTo(object)
					self.zazz:moveTo(object)
                    sfx.door:setVolume(0.2)
                    sfx.door:play()  -- door sound
				end
			end
		end
	end
end


function createGromato(x, y)

	local self = cs.states['play']

	if (self.player.score < 100) then

		table.insert(self.gromatoes, ent.gromato(x, y))
	end
end


-- On state kill
function PlayState:kill() end


-- Transfer data to state loading script
return PlayState
