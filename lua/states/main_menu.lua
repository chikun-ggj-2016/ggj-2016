-- chikun :: 2016
-- Main menu state


-- Temporary state, removed at end of script
local MainMenuState = class(PrototypeClass, function(new_class) end)


-- On state create
function MainMenuState:create()

	self.choices = {
		{
			text = "Start",
			image = gfx.utensils.fork
		},
		{
			text = "Options",
			image = gfx.utensils.knife
		},
		{
			text = "Credits",
			image = gfx.utensils.spoon
		}
	}
	
	self.current = 1
    cb.main_menu:setVolume(0.1)
    cb.main_menu:play()
end


-- On state update
function MainMenuState:update(dt)

	if (ci:wasPressed("left")) then

		self.current = math.max(1, self.current - 1)
        sfx.select:setVolume(0.1)
        sfx.select:play()
	elseif (ci:wasPressed("right")) then

		self.current = math.min(3, self.current + 1)
         sfx.select:setVolume(0.2)
         sfx.select:play()
	elseif (ci:wasPressed("a")) then
        sfx.ok:setVolume(0.2)
        sfx.ok:play()
        cb.main_menu:stop()
		if (self.current == 1) then

			cs:change('play')

		elseif (self.current == 2) then

			swapState(txt.intro)
		end
	end
end


function swapState(txt)

	prev_state = cs.current

	cs:load('text')
	cs.states['text'].text = txt


end
-- On state draw
function MainMenuState:draw()

	-- Draw main menu background image
	lg.setColor(255, 255, 255)
	lg.draw(gfx.bg.main_menu)

	lg.setFont(cf.mm_text)
	lg.printf("GRAND SALAD", 0, 8, 320, 'center')
	
	for key, choice in ipairs(self.choices) do

		-- Determine x and y positions
		local x, y = 44 + (key - 1) * 100, 72

		if (key == self.current) then

			lg.setColor(255, 255, 255, 192)
			lg.draw(gfx.utensils.spotlight, x - 8, y)
			lg.setColor(255, 255, 255)
			lg.draw(gfx.buttons[ci.scheme].a, x + 6, y + 72)
		else

			lg.setColor(255, 255, 255, 96)
		end

		-- Draw utensil
		lg.draw(choice.image, x, y)

		-- Draw choices text
		lg.setFont(cf.text)
		lg.setColor(0, 0, 0)
		lg.printf(choice.text, x - 15, y + 25, 64, 'center')
		lg.setColor(255, 255, 255)
		lg.printf(choice.text, x - 16, y + 24, 64, 'center')
	end
end


-- On state kill
function MainMenuState:kill() end


-- Transfer data to state loading script
return MainMenuState
