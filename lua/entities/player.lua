-- chikun :: 2016
-- Player


-- Player class
Player = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 14
		new_class.h = 22

		new_class.bosses_killed = 0

		new_class.score = 10
		new_class.invincible = 0

		new_class.dir      = 4.71		-- 270 degrees in radians
		new_class.dir_name = "down"

		new_class.anim_step = 0
		new_class.max_speed = 100

		new_class.unlocked = {
			fork  = false,
			knife = true,
			spoon = false
		}
	end)


-- Performed on player update
function Player:update(dt, collisions)

	if (lk.isDown('f5') and lk.isDown('lctrl')) then

		self.bosses_killed = 2
		cs.states['play']:setMap('carrot_9', 'lock_left')

		self.unlocked.knife = true
	end

	self.invincible = math.max(self.invincible - dt, 0)

	self:updateAttack(dt)

	if (ci:wasPressed('x')) then self:setAttack('spoon')
	elseif (ci:wasPressed('a')) then self:setAttack('knife')
	elseif (ci:wasPressed('b')) then self:setAttack('fork') end


	-- Figure out x and y axis of analog stick
	local axis_x, axis_y =
	    ci:getValue("right") - ci:getValue("left"),
	    ci:getValue("down")  - ci:getValue("up")

	-- Determine how far analog stick is pushed
	local dist = math.min(math.distance(0, 0, axis_x, axis_y), 1)

	-- If no movement required
	if (dist < 0.1) then

		-- Reset animation
		self.anim_step = 0

	-- If movement is on the table
	else

		-- Determine movement angle
		self.dir = math.angle(0, 0, axis_x, -axis_y)

		if (self.attack) then

			if (self.attack.name == "spoon") then

				dist = dist * 0.2
			end
		end

		-- Animate movement
		self.anim_step = (self.anim_step + (dt * 10 * dist)) % 4

		-- Determine speed of movement
		local speed = dist * dt * self.max_speed


		-- Move player and determine hitbox
		self.x = self.x + math.cos(self.dir) * speed
		local hitbox = self:getHitbox()
		local sign = math.sign(math.cos(self.dir))

		while (math.doesCollideTable(hitbox, collisions)) do

			if (sign == 1) then

				self.x = math.ceil(self.x)
			else

				self.x = math.floor(self.x)
			end

			self.x = self.x - sign
			hitbox = self:getHitbox()
		end


		-- Move player and determine hitbox
		self.y = self.y - math.sin(self.dir) * speed
		local hitbox = self:getHitbox()
		local sign = -math.sign(math.sin(self.dir))

		while (math.doesCollideTable(hitbox, collisions)) do

			if (sign == 1) then

				self.y = math.ceil(self.y)
			else

				self.y = math.floor(self.y)
			end

			self.y = self.y - sign
			hitbox = self:getHitbox()
		end
	end

	if (self.attack) then

		if (self.attack.name == "spoon") then

			self.dir = math.rad(270)
			self.dir_name = "down"
		end
	end

	local groms = cs.states['play'].gromatoes
	local to_remove = { }
	local hitbox = self:getHitbox()

	for key, gromato in ipairs(groms) do

		if (math.doesCollide(gromato, hitbox) and gromato.bouncing == 1) then

			table.insert(to_remove, key)

			self.score = self.score + 1
            sfx.collect_1:setVolume(0.1)
            sfx.collect_1:play()
		end
	end

	for i = #to_remove, 1, -1 do

		table.remove(groms, to_remove[i])
	end

	local enemies = cs.states['play'].enemies



		if (self.score == 0) then
            sfx.heartbeat:setVolume(0.2)
            sfx.heartbeat:play()
		else
            sfx.heartbeat:stop()
		end

	for key, enemy in ipairs(enemies) do

		if (math.doesCollide(hitbox, enemy) and self.invincible == 0 and enemy.bouncing == nil and not self:isShielded()) then

			self.invincible = 0.8

			if (self.score == 0) then

				self.score = 10 * (self.bosses_killed + 1)

				swapState(txt.death)
			else

				self.score = math.max(self.score - 10, 0)
			end


		end
	end
end



-- Performed on player draw
function Player:draw()

	-- Determine frame of player to be drawn
	local frame = math.floor(self.anim_step) + 1
	local alpha = 1

	if (self.invincible > 0) then

		alpha = math.round((self.invincible / 0.2) % 1)

        sfx.player_hurt:setVolume(0.2)
        sfx.player_hurt:play()
        else
        sfx.player_hurt:stop()
	end

	-- Draw self
	lg.setColor(255, 255, 255, alpha * 255)
	lg.draw(gfx.player[self:getDirName()][tostring(frame)], self.x, self.y)

	lg.setColor(255, 255, 255)
	self:drawAttack()
end


-- Get name of direction player is facing
function Player:getDirName()

	-- Determine direction in degrees
	local dir = math.deg(self.dir) % 360

	-- Return name of direction which player is facing
	if (dir > 315 or dir <= 45) then return "right"
	elseif (dir <= 135) then return "up"
	elseif (dir <= 225) then return "left"
	else return "down" end
end

function Player:getHitbox()

	return {
		x = self.x + 1,
		y = self.y + 10,
		w = 12,
		h = 12
	}
end

function Player:moveTo(object)

	if (object.w == 16 and object.h == 16) then

		self.x = object.x + object.w / 2 - self.w / 2
		self.y = object.y + object.h - self.h
	else

		self.x = object.x + object.w / 2 - self.w / 2
		self.y = object.y + object.h / 2 - self.h / 2
	end
end

function Player:setAttack(attack)

	-- Cancel if attack is already in motion
	if (self.attack) then return nil end

	if (attack == 'spoon' and self.unlocked.spoon) then

		self.attack = {
			time = 0.8,
			dir  = 0,
			name = 'spoon'
		}
        sfx.swing:setVolume(0.2)
        sfx.shield:play()  -- shield sound
	elseif (attack == 'knife' and self.unlocked.knife) then

		self.attack = {
			time = 0.3,
			dir  = self.dir,
			name = 'knife'
		}
        sfx.swing:setVolume(0.2)
        sfx.swing:play()  -- attack sound
	elseif (attack == 'fork'  and self.unlocked.fork) then

		self.attack = {
			time = 0.3,
			dir  = self.dir,
			name = 'fork'
		}
        sfx.swing:setVolume(0.2)
        sfx.swing:play()  -- attack sound
	end
end

function Player:updateAttack(dt)

	if (self.attack) then

		self.attack.time = self.attack.time - dt

		if (self.attack.name ~= 'spoon') then

			local enemies = cs.states['play'].enemies
			local hitbox = self:getHitbox()
			local x, y = hitbox.x + hitbox.w / 2, hitbox.y + hitbox.h / 2

			x = x + math.cos(self.attack.dir) * 11
			y = y - math.sin(self.attack.dir) * 11

			local box = {
				x = x - 3.5,
				y = y - 3.5,
				w = 7,
				h = 7
			}

			local to_remove = { }

			for key, enemy in ipairs(enemies) do

				if (math.doesCollide(enemy, box)) then
                    sfx.swing_hit:setVolume(0.2)
                    sfx.swing_hit:play()                    -- Player strikes enemy sound
					if (enemy:hurt(self.attack.name)) then

						table.insert(to_remove, key)
					end
				end
			end

			for i = #to_remove, 1, -1 do

				table.remove(enemies, to_remove[i])
			end
		else

			local enemies = cs.states['play'].enemies
			local hitbox = self:getHitbox()

			local box = {
				x = hitbox.x - 8,
				y = hitbox.y - 16,
				w = 32,
				h = 32
			}

			local to_remove = { }

			for key, enemy in ipairs(enemies) do

				if (math.doesCollide(enemy, box)) then
                    sfx.swing_hit:setVolume(0.5)
                    sfx.swing_hit:play()                    -- Player strikes enemy sound
					if (enemy.spoon) then

						table.insert(to_remove, key)
					end
				end
			end

			for i = #to_remove, 1, -1 do

				table.remove(enemies, to_remove[i])
			end
		end

		if (self.attack.time <= 0) then

			self.attack = nil
		end
	end
end


function Player:drawAttack()

	if (self.attack) then

		local hitbox = self:getHitbox()
		local x, y = hitbox.x + hitbox.w / 2, hitbox.y + hitbox.h / 2
		local dir = math.rad(360 - math.deg(self.attack.dir) + 90)

		if (self.attack.name == "spoon") then

			local alpha = (0.4 - math.abs(self.attack.time - 0.4)) / 0.4

			lg.setColor(255, 255, 255)
			lg.draw(gfx.weapon.spoon, self.x + 8, self.y)
			lg.setColor(255, 255, 47, 64 + 128 * alpha)
			lg.circle('fill', x, y, alpha * self.h / 2)

		elseif (self.attack.name == "fork") then

			local alpha = self.attack.time / 0.3

			lg.setColor(255, 255, 255, 255 * alpha)

			lg.draw(gfx.weapon.fork, x + math.sin(dir) * 16 * (1 - alpha),
			        y - math.cos(dir) * 16 * (1 - alpha), dir, 1, 1, 6, 10)

		elseif (self.attack.name == "knife") then

			local alpha = self.attack.time / 0.3

			lg.setColor(255, 255, 255, 255 * (0.5 - math.abs(alpha - 0.5)) * 2)

			dir = math.deg(dir) + 30 - 60 * alpha

			lg.draw(gfx.weapon.knife, x, y, math.rad(dir), 1, 1, 5, 18)
		end
	end
end


-- Is the player shielded from attacks?
function Player:isShielded()

	if (not self.attack) then return false end

	return self.attack.name == "spoon"
end


function Player:returnToMain()
    sfx.player_hurt:stop()
    sfx.player_dead:setVolume(0.2)
    sfx.player_dead:play()
	cs.states['play']:setMap('main_' .. tostring(self.bosses_killed + 1))
end
