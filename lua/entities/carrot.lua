-- chikun :: 2016
-- Carrot


-- Carrot class
Carrot = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 16
		new_class.h = 16

		new_class.y_offset = 16

		new_class.anim_step = 0
		new_class.facing = 'right'

		new_class.bounce_timer = math.random() * 1
		new_class.bounce_timer_max = 1
		new_class.new_x = 0
		new_class.new_y = 0
	end)


-- Performed on enemy update
function Carrot:update(dt, collisions)

	self.anim_step = (self.anim_step + dt * 8) % 5

	local room_w, room_h =
		cm.current.w * cm.current.tile_w,
		cm.current.h * cm.current.tile_h

	if (not self.bouncing) then

		self.bounce_timer = math.max(self.bounce_timer - dt, 0)

		if (self.bounce_timer == 0) then

			self.bouncing = 0
            sfx.door:setVolume(0.2)
            sfx.door:play()

			repeat
				self.new_x = math.random(room_w)
				self.new_y = math.random(room_h)

				hitbox = {
					x = self.new_x,
					y = self.new_y,
					w = self.w,
					h = self.h
				}
			until (not math.doesCollideTable(hitbox, collisions))

			if (self.new_x < self.x) then

				self.facing = 'left'
			else

				self.facing = 'right'
			end
		end

	else

		self.bouncing = self.bouncing + dt

		if (self.bouncing >= 1) then

			self.bouncing = nil
			self.bounce_timer = math.random() * self.bounce_timer_max
            sfx.door:setVolume(0.5)
            sfx.door:play()

			self.x = self.new_x
			self.y = self.new_y
		end
	end
end


-- Performed on enemy draw
function Carrot:draw()

	-- Determine frame of enemy to be drawn
	local frame = math.floor(self.anim_step) + 1
	local rot = math.rad(math.sin(360 * self.anim_step / 5) * 2)

	local x = self.x
	local y = self.y

	if (self.bouncing) then

		local scal = math.sin(math.rad(self.bouncing * 180))
		x = x + (self.new_x - self.x) * self.bouncing
		y = y + (self.new_y - self.y) * self.bouncing - scal * 24
	end

	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.npc.carrot[self.facing][tostring(frame)], x + self.w / 2, y,
	        rot, 1, 1, self.w / 2, 16)
end


function Carrot:hurt(hurt_type)

	if (self.bouncing == nil) then

		createGromato(self.x + self.w / 2 - 4, self.y + self.h / 2 - 4)

		return true
	end

	return false
end


return Carrot
