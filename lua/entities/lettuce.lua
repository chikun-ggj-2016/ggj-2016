-- chikun :: 2016
-- Lettuce


-- Lettuce class
Lettuce = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 32
		new_class.h = 16

		new_class.y_offset = 16

		new_class.x_speed = 48
		new_class.y_speed = 20

		new_class.anim_step = 0
		new_class.facing = 'right'
	end)


-- Performed on enemy update
function Lettuce:update(dt, collisions)

	self.anim_step = (self.anim_step + dt * 8) % 5

	self.x = self.x + dt * self.x_speed

	if (math.doesCollideTable(self, collisions)) then

		self.x_speed = -self.x_speed

		repeat
			self.x = self.x + math.sign(self.x_speed)
		until (not math.doesCollideTable(self, collisions))
	end

	self.facing = 'left'
	if (self.x_speed > 0) then

		self.facing = 'right'
	end


	self.y = self.y + dt * self.y_speed

	if (math.doesCollideTable(self, collisions)) then

		self.y_speed = -self.y_speed

		repeat
			self.y = self.y + math.sign(self.y_speed)
		until (not math.doesCollideTable(self, collisions))
	end
end


-- Performed on enemy draw
function Lettuce:draw()

	-- Determine frame of enemy to be drawn
	local frame = math.floor(self.anim_step) + 1

	local x = self.x
	local y = self.y


	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.npc.lettuce[self.facing][tostring(frame)], x + self.w / 2 - 8, y,
	        0, 1, 1, self.w / 2, 16)
end


function Lettuce:hurt(hurt_type)

	createGromato(self.x + self.w / 2 - 4, self.y + self.h / 2 - 4)

	return true
end


return Lettuce
