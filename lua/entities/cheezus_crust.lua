-- chikun :: 2016
-- Tomato


-- CheezusCrust class
CheezusCrust = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 32
		new_class.h = 32

		new_class.y_offset = 32

		new_class.x_speed = -90.1993
		new_class.y_speed = 35.1994

		new_class.anim_step = 0
		new_class.facing = 'right'

		new_class.invincible = 0
		new_class.health = 3

		new_class.image = gfx.npc.cheesus_crust
	end)


-- Performed on enemy update
function CheezusCrust:update(dt, collisions)

	self.anim_step = (self.anim_step + dt * 8) % 5

	self.invincible = math.max(self.invincible - dt, 0)

	self.x = self.x + dt * self.x_speed

	if (math.doesCollideTable(self, collisions)) then

		self.x_speed = -self.x_speed

		repeat
			self.x = self.x + math.sign(self.x_speed)
		until (not math.doesCollideTable(self, collisions))
	end

	self.facing = 'left'
	if (self.x_speed > 0) then

		self.facing = 'right'
	end


	self.y = self.y + dt * self.y_speed

	if (math.doesCollideTable(self, collisions)) then

		self.y_speed = -self.y_speed

		repeat
			self.y = self.y + math.sign(self.y_speed)
		until (not math.doesCollideTable(self, collisions))
	end
end


-- Performed on enemy draw
function CheezusCrust:draw()

	-- Determine frame of enemy to be drawn
	local frame = math.floor(self.anim_step) + 1
	local alpha = 1

	if (self.invincible > 0) then

		alpha = math.round((self.invincible / 0.2) % 1)
	end

	local x = self.x
	local y = self.y

	local scale = 1
	if (self.facing == 'right') then

		scale = -1
	end

	-- Draw self
	lg.setColor(255, 255, 255, alpha* 255)
	lg.draw(self.image, x + self.w / 2, y, 0, scale, 1, 32, 32)
end


function CheezusCrust:hurt(hurt_type)

	if (self.invincible == 0) then

		self.health = self.health - 1

		self.invincible = 0.8

		if (self.health == 0) then

			local state = cs.states['play']

			state.player.score = state.player.score + 20

			state.player.bosses_killed = 2

			state:setMap('main_3')

			return true
		else

			return false
		end

	end

	return false
end


return CheezusCrust
