-- chikun :: 2016
-- GFork


-- GFork class
GFork = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 16
		new_class.h = 16

		new_class.speed = 75

		new_class.y_offset = 16
		new_class.dir = math.random(1, 360) % 360
	end)


-- Performed on enemy update
function GFork:update(dt, collisions)

	while (math.doesCollideTable(self:makeTable(dt), collisions)) do

		self.dir = (self.dir - math.random(45, 90)) % 360
	end

	local tab = self:makeTable(dt)
	self.x = tab.x
	self.y = tab.y
end

function GFork:makeTable(dt)

	return {
		x = self.x + math.cos(math.rad(self.dir)) * self.speed * dt,
		y = self.y - math.sin(math.rad(self.dir)) * self.speed * dt,
		w = self.w,
		h = self.h
	}
end


-- Performed on enemy draw
function GFork:draw()

	x = self.x
	y = self.y - self.y_offset

	local facing = 'left'
	if (math.cos(math.rad(self.dir)) > 0) then
		facing = 'right'
	end

	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.npc.spirits.fork[facing], x, y)
end


function GFork:hurt(hurt_type)

	if (hurt_type == 'fork') then

		createGromato(self.x + self.w / 2 - 4, self.y + self.h / 2 - 4)

		return true
	end

	return false
end


return GFork
