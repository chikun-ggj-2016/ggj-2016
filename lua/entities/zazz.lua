-- chikun :: 2016
-- Zazz


--Zazz class
Zazz = class(function(new_class, x, y)

	  local player = cs.states['play'].player

	  new_class.x = player.x + (player.y/2)
	  new_class.y = player.y + 6

	  new_class.w = 9
	  new_class.h = 9

	  new_class.anim_step = 0
	  new_class.max_speed = 100

	  new_class.dir_name = "firefly"
		new_class.dir = 0

	end)

-- Performed on zazz update
function Zazz:update(dt)

	self.dir = (self.dir + 180 * dt) % 360

	local player = cs.states['play'].player
	local dir = math.rad(self.dir)

	self.x = player.x + player.w / 2 - 4.5 + 16*math.sin(dir)
	self.y = player.y - (player.w /2) + 3*math.sin(dir*2)

end

-- performed on Zazz to be drawn
function Zazz:draw()

	-- determine from of zazz to be drawn
	local frame = math.floor(self.anim_step) + 1


	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.firefly[tostring(frame)], self.x, self.y)

end

function Zazz:distFromPlayer()

	local x_diff = player.x - self.x
	local y_diff = player.y - self.y

end

function Zazz:moveTo(object)

	self.x = object.x + object.w / 2 - self.w / 2
	self.y = object.y + object.h / 2 - self.h / 2

end
