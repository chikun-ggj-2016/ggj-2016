-- chikun :: 2016
-- Load all entities

local current_dir = ...

ent = { }

-- Get a table of files / subdirs from dir
local items = love.filesystem.getDirectoryItems(...)

-- Iterate through all files and load them into states
for key, val in ipairs(items) do

	-- Load file as long as it isn't init.lua
	if (val ~= "init.lua") then

		local name = val:sub(1, -5)
		ent[name] = require(current_dir .. "/" .. name)
	end
end
