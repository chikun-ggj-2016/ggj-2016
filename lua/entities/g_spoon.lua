-- chikun :: 2016
-- GSpoon


-- GSpoon class
GSpoon = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 16
		new_class.h = 16

		new_class.speed = 75

		new_class.y_offset = 16
		new_class.dir = math.random(1, 360) % 360
	end)


-- Performed on enemy update
function GSpoon:update(dt, collisions)

	local hb = cs.states['play'].player:getHitbox()

	local dir = math.angle(self.x + 8, self.y + 8, hb.x + hb.w / 2, hb.y + hb.h / 2)

	if (not math.doesCollideTable({
					x = self.x - math.cos(dir) * 20 * dt,
					y = self.y,
					w = 16,
					h = 16
				}, collisions)) then
		self.x = self.x - math.cos(dir) * 20 * dt
	end

	if (not math.doesCollideTable({
					x = self.x,
					y = self.y - math.sin(dir) * 20 * dt,
					w = 16,
					h = 16
				}, collisions)) then
		self.y = self.y - math.sin(dir) * 20 * dt
	end
end


-- Performed on enemy draw
function GSpoon:draw()

	x = self.x
	y = self.y - self.y_offset

	local facing = 'left'
	if (math.cos(math.rad(self.dir)) > 0) then
		facing = 'right'
	end

	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.npc.spirits.spoon[facing], x, y)
end


function GSpoon:hurt(hurt_type)

	return false
end

function GSpoon:spoon()

	createGromato(self.x + self.w / 2 - 4, self.y + self.h / 2 - 4)

	return true
end


return GSpoon
