-- chikun :: 2016
-- Gromato


-- Gromato class
Gromato = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 8
		new_class.h = 8
		new_class.bouncing = 0
	end)


-- Performed on enemy update
function Gromato:update(dt)

	self.bouncing = math.min(self.bouncing + dt * 2, 1)
end


-- Performed on enemy draw
function Gromato:draw()

	local scal = math.sin(math.rad(self.bouncing * 180))
	x = self.x
	y = self.y - scal * 32

	-- Draw self
	lg.setColor(255, 255, 255)
	lg.draw(gfx.items.grape_tomato, x, y)
end


function Gromato:hurt(hurt_type)

	return self.bouncing == nil
end


return Gromato
