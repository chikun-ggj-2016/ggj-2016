-- chikun :: 2016
-- Tomato


-- HeadLettuce class
HeadLettuce = class(function(new_class, x, y)

		new_class.x = x or 0
		new_class.y = y or 0

		new_class.w = 32
		new_class.h = 32

		new_class.y_offset = 32
		new_class.timer = 2

		new_class.x_speed = 0
		new_class.y_speed = 0

		new_class.dir = 0

		new_class.invincible = 0
		new_class.health = 3

		new_class.image = gfx.npc.head_lettuce

		swapState(txt.lettuce_boss)
	end)


-- Performed on enemy update
function HeadLettuce:update(dt, collisions)

	self.timer = math.max(self.timer - dt, 0)

	local player = cs.states['play'].player

	if (self.timer == 0 and self.x_speed == 0 and self.y_speed == 0) then

		local ang = math.angle(self.x + self.w, self.y, player.x + player.w / 2, player.y + player.h / 2)
		self.x_speed = math.cos(ang) * 200
		self.y_speed = math.sin(ang) * 150
	end

	local stopped = false

	self.invincible = math.max(self.invincible - dt, 0)

	self.x = self.x + dt * self.x_speed

	self.dir = (self.dir + self.x_speed * dt) % 360

	if (math.doesCollideTable(self, collisions)) then

		self.x_speed = -self.x_speed

		repeat
			self.x = self.x + math.sign(self.x_speed)
		until (not math.doesCollideTable(self, collisions))

		stopped = true
	end

	self.facing = 'left'
	if (self.x_speed > 0) then

		self.facing = 'right'
	end


	self.y = self.y + dt * self.y_speed

	if (math.doesCollideTable(self, collisions)) then

		self.y_speed = -self.y_speed

		repeat
			self.y = self.y + math.sign(self.y_speed)
		until (not math.doesCollideTable(self, collisions))

		stopped = true
	end

	if (stopped) then

		self.x_speed = 0
		self.y_speed = 0

		self.timer = 2
	end
end


-- Performed on enemy draw
function HeadLettuce:draw()

	-- Determine frame of enemy to be drawn
	local alpha = 1

	if (self.invincible > 0) then

		alpha = math.round((self.invincible / 0.2) % 1)
	end

	local x = self.x
	local y = self.y

	local scale = 1
	if (self.facing == 'right') then

		scale = -1
	end

	-- Draw self
	lg.setColor(255, 255, 255, alpha* 255)
	lg.draw(self.image, x + self.w / 2, y, math.rad(self.dir), scale, 1, 32, 32)
end


function HeadLettuce:hurt(hurt_type)

	if (self.invincible == 0) then

		self.health = self.health - 1

		self.invincible = 0.8

		if (self.health == 0) then

			local state = cs.states['play']

			state.player.score = state.player.score + 40

			state.player.bosses_killed = 4

			state:setMap('main_5')

			return true
		else

			return false
		end

	end

	return false
end


return HeadLettuce
