-- chikun :: 2014-2016
-- Tiled .lua map loading tool


-- Table containing map functions and variables
local map = {
	current = nil
}

local current_dir = ...


-- Loads and returns a map
function map.load(map_name)

	-- Load map into a table
	local map_data = require(current_dir .. "/" .. map_name)

	-- New map structure
	local new_map = {
		layers    = {},
		quads     = {},
		tilesets  = {},
		w         = map_data.width,
		h         = map_data.height,
		tile_w    = map_data.tilewidth,
		tile_h    = map_data.tileheight,
		name      = map_name,
		objects   = {}
	}

	-- Loads all quads from the map data
	for key, ext_tileset in ipairs(map_data.tilesets) do

		-- Our new tileset <3
		local n = {}

		-- Interpret tileset's image into an actual loaded image
		n.image = getResourceFromString(ext_tileset.image)

		-- Transfer certain values to the new tileset
		n.gid     = ext_tileset.firstgid
		n.image_w = ext_tileset.imagewidth
		n.image_h = ext_tileset.imageheight
		n.margin  = ext_tileset.margin
		n.spacing = ext_tileset.spacing
		n.tile_w  = ext_tileset.tilewidth
		n.tile_h  = ext_tileset.tileheight

		-- Determine quad value range
		local repeat_x = math.ceil((n.image_w - n.spacing) / n.tile_w) - 1
		local repeat_y = math.ceil((n.image_h - n.spacing) / n.tile_h) - 1
		local y_pos    = n.margin

		-- Start counting at current gid
		local gid_q = n.gid

		-- Interpret quads from the tileset
		for current_y = 0, repeat_y do

			-- Reset x starting position
			local x_pos = n.margin

			for current_x = 0, repeat_x do

				-- Add current quad to the table
				new_map.quads[gid_q] =
				love.graphics.newQuad(x_pos + (current_x * n.tile_w),
				                      y_pos + (current_y * n.tile_h),
				                      n.tile_w, n.tile_h, n.image_w, n.image_h)

				-- Increment x starting position
				x_pos = x_pos + n.spacing

				-- Increment quad number
				gid_q = gid_q + 1
			end

			-- Increment y starting position
			y_pos = y_pos + n.spacing
		end

		-- Add to the list of map tilesets
		table.insert(new_map.tilesets, n)
	end


	-- Load layers into tables
	for key, layer in ipairs(map_data.layers) do

		-- Create new table based on layer name
		local new_layer = {
			data    = layer.data,
			name    = layer.name,
			type    = layer.type,
			vars    = layer.properties,
			w       = layer.width,
			h       = layer.height
		}

		-- Create table for objects
		local new_objects = { }

		-- If layer isn't tilelayer, import objects
		if (layer.type ~= "tilelayer") then

			for key, object in ipairs(layer.objects) do

				-- Get data from object's quad, if available
				local quad_vars = {}

				if (object.gid) then

					if (object.gid > 1000) then

						object.gid = nil
					else

						quad_vars = {new_map.quads[object.gid]:getViewport()}
					end
				end

				-- Add object to table
				new_objects[#new_objects+1] = {
					name = object.name,
					type = object.type,
					gid  = object.gid,
					w    = (quad_vars[3] or object.width),
					h    = (quad_vars[4] or object.height),
					x    = object.x,
					y    = object.y - (quad_vars[4] or 0),
					ox   = object.x,
					oy   = object.y - (quad_vars[4] or 0),
					prop = object.properties
				}
			end
		end

		-- Add new layer to map_data
		if (layer.name == "fg") then

			new_map.fg = new_objects
		else

			new_layer.objects = new_objects

			table.insert(new_map.layers, new_layer)
		end
	end

	-- Return the newly created map
	return new_map
end


-- Draws the current or given map
function map.draw(given_map)

	-- If map isn't given, default to current
	local u = given_map or map.current

	-- Draw all layers
	for key, layer in ipairs(u.layers) do

		-- If tile layer...
		if (layer.type == "tilelayer") then

			-- ...then draw all tiles in layer
			for key, tile in ipairs(layer.data) do

				if (tile > 0) then

					-- Calculated placement of tile
					local tmp_y = math.floor((key - 1) / u.w)
					local tmp_x = key - (tmp_y * u.w) - 1

					local quad_vars = {u.quads[tile]:getViewport()}

					-- If tile exists, draw it
					if (tile > 0) then

						love.graphics.draw(getImageFromGID(tile, u),
						                   u.quads[tile], tmp_x * u.tile_w,
						                   tmp_y * u.tile_h -
						                   quad_vars[4] + u.tile_h)
					end
				end
			end
		else

			-- ...draw all objects in layer...
			for key, object in ipairs(layer.objects) do

				-- ...if that's possible
				if (object.gid) then

					love.graphics.draw(getImageFromGID(object.gid, u),
					                   u.quads[object.gid], object.x, object.y)
				end
			end
		end
	end
end

--[[
	Draw the foreground layer
	INPUT:  Map to draw (if ommited, map.current)
]]
function map.drawFG(given_map)

	local map = given_map or map.current

	if (map.fg) then

		-- Check for foreground layer
		for key, object in ipairs(map.fg) do

			-- Draw if possible
			if (object.gid) then

				love.graphics.draw(getImageFromGID(object.gid, map),
				                   map.quads[object.gid], object.x, object.y)
			end
		end
	end
end


-- Returns the image used by a certain gid
function getImageFromGID(gid, given_map)

	-- If map isn't given, default to current
	local u = given_map or map.current

	-- Count current number of tilesets
	local num = #u.tilesets

	-- Seek backwards through tilesets until desired image found
	while (gid < u.tilesets[num].gid) do

		num = num - 1
	end

	-- Return found image
	return u.tilesets[num].image
end


--[[
	Convert resource from string to actual data
	  eg. 'gfx.shrek' to gfx.shrek's image data
	INPUT:  File name of resource to be converted.
	OUTPUT: Resource that has been found.
]]
function getResourceFromString(resource)

	-- Remove unneccessary parts of filename
	resource = resource:sub(1, -5)
	resource = resource:gsub("%.%./", "")

	-- Replace slashes with periods to simplify loading
	resource = resource:gsub("/", "%.")

	-- Convert current string to table of strings
	tab = {}
	for word in resource:gmatch("([^.]+)") do

		tab[#tab + 1] = word
	end

	-- Recursively access deeper parts of table containing resource
	for key, value in ipairs(tab) do

		if (key == 1) then

			resource = _G[value]
		else

			resource = resource[value]
		end
	end

	-- Return the found resource
	return resource
end


-- Recursively checks a folder for maps and adds any found to a maps table
local function loadMapsFolder(dir, tab)

    local tab = tab or {}

    -- Get a table of files / subdirs from dir
    local items = love.filesystem.getDirectoryItems(dir)

    for key, val in ipairs(items) do

		-- Remove ".lua" extension on file
		local name = val:sub(1, -5)

		if (name ~= "init") then

			-- Load map into table
			tab[name] = map.load(name)
		end
    end

    return tab
end


-- Load the maps folder into the maps table
maps = loadMapsFolder(...)

return map
