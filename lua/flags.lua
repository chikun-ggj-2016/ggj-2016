-- chikun :: 2015-2016
-- Game flags


DEFAULT_SCALE = 3
FULLSCREEN    = false
GAME_VSYNC    = true
GAME_WIDTH    = 320
GAME_HEIGHT   = 180

IS_OUYA        = false  -- Whether or not the game is running on an OUYA
IS_PIXEL_BASED = true   -- Whether or not the game is pixel based


local current_dir = ...

-- Load test flags is they exist on the system
if (love.filesystem.exists(current_dir .. "/test_flags.lua")) then

	require(current_dir .. "test_flags")
end
