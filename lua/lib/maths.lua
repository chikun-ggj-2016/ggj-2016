-- chikun :: 2014-2015
-- Extra maths functions


-- Seed the random number generator
math.randomseed(os.time())


--[[
	Calculates the angle from one point to another.
	INPUT:  x1 and y1 are the coordinates of the first point.
	        x2 and y2 are the coordinates of the second point.
	OUTPUT: The angle from point1 to point 2 in radians.
]]
function math.angle(x1, y1, x2, y2)

	return math.atan2(y2 - y1, x2 - x1)
end


--[[
	Constrains a value to a minimum and a maximum.
	INPUT:  Three numbers, min, the value to be clamped, and the max.
	OUTPUT: If the value is smaller than min, returns min.
	        If the value is larger then max, returns max.
	        Otherwise returns value.
]]
function math.clamp(min, value, max)

	return math.max(min, math.min(max, value))
end


--[[
	Calculates the distance from one point to another.
	INPUT:  x1 and y1 are the coordinates of the first point.
	        x2 and y2 are the coordinates of the second point.
	OUTPUT: The distance from point1 to point 2.
]]
function math.distance(x1, y1, x2, y2)

	return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5
end


--[[
	Returns the value of an arg rounded to dec decimal places, 0 if omitted.
	INPUT:  The value to be rounded and the number of decimal places
	        to round to.
	OUTPUT: value rounded to dec decimal places.
	NOTE:   Negative dec results in rounding behind the decimals
	          eg. math.round(4777, -2) == 4800
]]
function math.round(value, dec)

	dec = 10 ^ (dec or 0)
	return math.floor(value * dec + 0.5) / dec
end


--[[
	Returns the sign of a value.
	INPUT:  value is the number to be checked.
	        preventZero will turn 0 into 1.
	OUTPUT: If value is smaller than 0, returns -1.
	        If value is larger than 0, returns 1.
	        If value is equal to 0, returns 0.
]]
function math.sign(value, prevent_zero)

	if (value == 0) then

		return prevent_zero and 1 or 0
	else

		return value > 0 and 1 or -1
	end
end

function math.doesCollide(obj_1, obj_2)

	return obj_1.x < obj_2.x + obj_2.w and
	       obj_2.x < obj_1.x + obj_1.w and
	       obj_1.y < obj_2.y + obj_2.h and
	       obj_2.y < obj_1.y + obj_1.h
end


-- Determine player's horizontal hitbox
function math.doesCollideTable(obj, tab)

	for key, box in ipairs(tab) do

		-- Check for collisions
		if (math.doesCollide(box, obj)) then

			return true
		end
	end

	return false
end
