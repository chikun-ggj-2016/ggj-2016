-- chikun :: 2015
-- Basic input management system


-- Get current directory
local current_dir = ...


-- Require love interceptors
require (... .. "/love_input")


local InputController = class(function(new_controller)

		-- All schemes which can be used in different scenarios
		new_controller.schemes = {
			keys = require(current_dir .. "/layout_keys"),
			ouya = require(current_dir .. "/layout_ouya"),
			xbox = require(current_dir .. "/layout_xbox")
		}

		-- Deadzone of controller analog sticks
		new_controller.deadzone = 0.3

		-- Determine default control scheme
		if (IS_OUYA) then

			new_controller.scheme = "ouya"
		elseif (lj.getJoystickCount() > 0) then

			new_controller.scheme = "xbox"
		else

			new_controller.scheme = "keys"
		end


		-- Whether an input is pressed, has been pressed, or has been released
		new_controller.inputs   = {}
		new_controller.pressed  = {}
		new_controller.released = {}
	end)


--[[
	Updates InputController.
]]
function InputController:update()

	-- Blank out pressed/released tables
	self.pressed = {}
	self.released = {}

	-- Local max to reduce execution time
	local max = math.max

	-- Iterate through all control types in the scheme file
	for control, keys in pairs(self.schemes[self.scheme]) do

		-- Determine previous value for control and make variable for new
		local value_prev, value = (self.inputs[control] or 0), 0

		-- Iterate through all keys for current control and find max
		for k, key in ipairs(keys) do

			value = max(value, self.inputs[key] or 0)
		end

		-- Restrict value to deadzone
		if (value < self.deadzone) then

			value = 0
		end


		-- Determine if key was pressed or was released and act if needed
		if (value_prev == 0 and value > 0) then

			table.insert(self.pressed, control)
		elseif (value_prev > 0 and value == 0) then

			table.insert(self.released, control)
		end


		-- Update inputs table
		self.inputs[control] = value
	end
end


--[[
	Returns the value of an input (number).
	INPUT:  control is the control to get value of.
	OUTPUT: (number) Value of input currently.
]]
function InputController:getValue(control)

	return self.inputs[control] or 0
end


--[[
	Returns whether an input is pressed (boolean).
	INPUT:  control is the control to get value of.
	OUTPUT: (boolean) Whether input is pressed.
]]
function InputController:isDown(control)

	return (self.inputs[control] or 0) >= self.deadzone
end


--[[
	Sets the current scheme of the InputController.
	INPUT: Name of scheme which will be used
]]
function InputController:setScheme(scheme)

	self.scheme = scheme
end


--[[
	Returns whether a control has been pressed since last update.
	INPUT:  Control to check for.
	OUTPUT: Whether the control was pressed.
]]
function InputController:wasPressed(control)

	for key, value in ipairs(self.pressed) do

		if (value == control) then

			return true
		end
	end

	return false
end


--[[
	Returns whether a control has been released since last update.
	INPUT:  Control to check for.
	OUTPUT: Whether the control was released.
]]
function InputController:wasReleased(control)

	for key, value in ipairs(self.released) do

		if (value == control) then

			return true
		end
	end

	return false
end


return InputController()
