-- chikun :: 2015-2016
-- Control scheme for OUYA controller

return {
	up    = { "joy_button_12", "joy_axis_2_neg" },
	left  = { "joy_button_14", "joy_axis_1_neg" },
	down  = { "joy_button_13", "joy_axis_2_pos" },
	right = { "joy_button_15", "joy_axis_1_pos" },
	a     = { "joy_button_1" },
	b     = { "joy_button_2" },
	x     = { "joy_button_3" },
	y     = { "joy_button_4" },
	start = { "key_menu" }
}
