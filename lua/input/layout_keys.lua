-- chikun :: 2015-2016
-- Control scheme for keyboard

return {
	up    = { "key_w", "key_i", "key_up" },
	left  = { "key_a", "key_j", "key_left" },
	down  = { "key_s", "key_k", "key_down" },
	right = { "key_d", "key_l", "key_right" },
	a     = { "key_z" },
	b     = { "key_x" },
	x     = { "key_c" },
	y     = { "key_space" },
	start = { "key_c", "key_escape", "key_lshift" }
}
