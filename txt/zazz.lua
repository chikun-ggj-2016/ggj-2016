return {
	{
		{
			text = "I don't find these vegetables very apPEELing!",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "... Get it?",
			background = "clear",
			speaker = "Zazz"
		},
	},
    {
		{
			text = "Do you want to hear a non-sensical pun?",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "Asparagus.",
			background = "clear",
			speaker = "Zazz"
		},
	},
    {
		{
			text = "Blessed be the brave in these hard times ...",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "In the name of the one true god, lettuce pray.",
			background = "clear",
			speaker = "Zazz"
		},
	},
	{
		{
			text = "I bet you think I'm a firefly.",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "I'm actually a regular fly holding a glowstick.",
			background = "clear",
			speaker = "Zazz"
		},
	},
    {
		{
			text = "Things were nicer here 1000 years ago.",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "Ah, to be back in my salad days again...",
			background = "clear",
			speaker = "Zazz"
		},
	},
    {
		{
			text = "I have the most incredible engagement ring to give to my lady,",
			background = "clear",
			speaker = "Zazz"
		},
        {
			text = "Well... when I build up the courage...",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "It's 24 carrot gold! Hah! Ahahahah! Got you!",
			background = "clear",
			speaker = "Zazz"
		},
        {
			text = "Everyone I love leaves me...",
			background = "clear",
			speaker = "Zazz"
		},
	},

    {
		{
			text = "Slow down! These wings aren't built for speed...",
			background = "clear",
			speaker = "Zazz"
		},
        {
			text = "You're so much faster than me...",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "I can barely ketchup!",
			background = "clear",
			speaker = "Zazz"
		},
	},
    {
		{
			text = "...AND SO THE LORD MADE THE PRODUCIANS IN HIS FORM...",
			background = "clear",
			speaker = "Zazz"
		},
        		{
			text = "...THAT MAN COULD THRIVE ON THEIR NUTRIENTS...",
			background = "clear",
			speaker = "Zazz"
		},
		{
			text = "------------------- The Book Of Mathew, 21",
			background = "clear",
			speaker = "Zazz"
		},
	},
}
