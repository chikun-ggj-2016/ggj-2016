return {
	{
		text = "Thousands of years ago, an ancient underground\n" ..
		       "society known as Cutleria ruled over the world.",
		background = gfx.bg.mountain,
		speaker = "none",
		music = cb.intro
	},
	{
		text = "Its citizens, known as Cutlerians, would hunt the race\n" ..
		       "known as Producians."
	},
	{
		text = "They would then turn these Producians into food and\n" ..
		       "adequately feed the entire world.",
		background = gfx.bg.test_intro_2
	},
	{
		text = "Near the end of their civilisation, the Cutlerians\n" ..
		       "divided into three cults: Fork, Spoon, and Knife.",
		background = gfx.bg.test_intro_3
	},
	{
		text = "The Cutlerians were so concerned with fighting that\n" ..
		       "they forgot to feed the world, and also themselves."
	},
	{
		text = "This spelled their demise and today, ghosts of the\n" ..
		       "three cults roam the old city.",
		background = gfx.bg.blank
	},
	{
		text = "Every 1000 years since, the world has sent one\n" ..
		       "person to Cutleria to prepare a feast.",
		background = gfx.bg.mountain
	},
	{
		text = "The feast this person prepares will feed the planet\n" ..
		       "for a millenium."
	},
	{
		text = "And this time, you are that person.",
		background = gfx.bg.test_intro_4
	},
	{
		text = "Initiate! You must now enter the mountain and save\n" ..
				"the world.",
		background = gfx.bg.cave_old_man,
		speaker = "Old Priest"
	},
	{
		text = "To guide you on your way, a wisp will accompany you."
	},
	{
		text = "Now go! FEED US!"
	},
	{
		text = "The priest pushes you into the mountain.",
		speaker = "",
		background = gfx.bg.falling
	},
	{
		text = "...",
         newmusic = cb.intro,
        music = cb.peaceful_2,
		background = gfx.bg.blank
	},
	{
		text = "Hey! Wake up!",

		background = gfx.bg.blank
	},
	{
		text = "What's your name?",
		speaker = "...",
		choices = {"Alex", "Jamie", "Rod"},
		code = function(choice)
			if (choice == 1) then
				cs.states['text']:setPlayerName("Alex")
			elseif (choice == 2) then
				cs.states['text']:setPlayerName("Jamie")
			else
				cs.states['text']:setPlayerName("Rod")
			end
		end

	},
	{
		text = "Hi ~~~. My name is Zazz and now we must\n" ..
			   "go quickly and complete our quest",
		speaker = "Zazz",
		background = gfx.bg.zazz_cave
	},
	{
		text = "or we shall be trapped here forever\n" ..
			   "and everyone will starve"
	},
	{
		text = "First we must collect the three Utensils Of Destiny\n" ..
			   "before we can hope to vanquish all the"
	},
	{
		text = "Producian Guardians and complete the Ritual\n" ..
			   "of the Grand Salad and save the world!",
        --newmusic = cb.peaceful_2
	}
}
