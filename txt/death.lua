return {
	{
		text = "Exhausted, you return to the cauldron.",
		background = "clear",
		code = function()
			cs.states['play'].player:returnToMain()
		end

	}
}
