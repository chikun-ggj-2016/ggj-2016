return {
	{
		text = "With the creation of the Grand Salad\n" ..
			   "the world was once again free of fear",
		background = gfx.bg.bowl_full,
		music = cb.peaceful
	},
	{
		text = "from starvation for a millenia once more.\n" ..
			   "And so the people of Cutleria were once again"
	},
	{
		text = "able to live their live in peace and take\n" ..
			   "part in as much salad as they could ever need"
	},
	{
		text = "Until in the new millenia they require once\n" ..
			   "again for a pilgrimage to be made again!",
		code = function() swapState(txt.credits) end
	}
}
