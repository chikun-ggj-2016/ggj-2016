return {
	{
		text = "Oh! HI, wanna be my friend?",
		speaker = "Cheerful Carrot",
        newmusic = cb.carrot_dungeon,
        music = cb.carrot_boss,
		background = gfx.carrot_death.carrot_sm
	},
	{
		text = "You're coming closer?\n" ..
			   "you want a hug?",
		background = gfx.carrot_death.carrot_med
	},
	{
		text = "What's that youve got? A knife! Careful\n" ..
			   "Are we making a meal together?",
		background = gfx.carrot_death["1"]
	},
	{
		text = "OW! What are you doing?!",
		background = gfx.carrot_death["2"]
	},
	{
		text = "Argh!! ...m-m-mo-MONSTER!\n" ..
			   "ARRRGGGGGGGHHHHHHH...",
		background = gfx.carrot_death["3"],
		code = function()
			local state = cs.states['play']

			state.player.score = state.player.score + 30

			state.player.bosses_killed = 3

			state:setMap('main_4')
		end
	}
}
